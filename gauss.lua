require 'nn'
require 'nngraph'
require 'rnn'
require 'cunn'

require '1_data'
require '3_loss'
require 'minibatch'
require 'mdn'

cmd = torch.CmdLine()
cmd:option('-model', 'results/mdn-linear/model.net','path to the input model file')
cmd:option('-seed', 5, 'random number generator seed')
cmd:option('-length', 20, 'number of frames to generate')
cmd:option('-output', 'test.wav', 'path to the output audio file')
cmd:option('-bias', 1, 'sample variance scalar')
opt = cmd:parse(arg)

torch.setdefaulttensortype('torch.FloatTensor')
--torch.manualSeed(opt.seed)

model = torch.load(opt.model)
model:evaluate()
model:forget()

-- seeding
seedlen = 100
-- sequence generation
local seq = {}


-- make a batch with sequence length seedlen and batch size 1
local x, _ = makebatch(data.inputs,data.targets,1,1,1)
for t = 1, seedlen do
    print(model:forward(x)[1])
    x, _ = makebatch(data.inputs,data.targets,t,1,1)
    seq[t] = x[1][1][1]
end



for t = 1+seedlen, opt.length+seedlen do
    --print("Input:")
    --print(x[1])
    out = model:forward(x)
    seq[t] = criterion.criterion:sample(out[1], opt.bias):clone()[1][1]
    print(seq[t])
    --print("Output:")
    --print(seq[t][1])
    x = {torch.FloatTensor({{torch.random(2)}})}

    --x = seq[t]
end

print("Sequence:")
for i=1,seedlen+opt.length do
  print(seq[i])
end


