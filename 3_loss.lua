-- Define the loss function.

require 'torch'   -- torch
require 'nn'      -- provides all sorts of loss functions
require 'mdn'

---------------------------------------------------------------------

-- MDN
criterion = nn.SequencerCriterion(nn.MDNCriterion(opt.mdn))


if opt.useCuda == 1 then
  print('pushing criterion to GPU')
  criterion:cuda()
end



----------------------------------------------------------------------
print '==> here is the loss function:'
print(criterion)
