from pylab import *
import numpy as np
from scipy.io import wavfile
import argparse

parser = argparse.ArgumentParser(description='Convert torch tensor generated ' +
  'by the RNN to .wav file.')
parser.add_argument('-input',type=str,default='sample.txt',help='Filename of input file')
parser.add_argument('-output',type=str,default='final_out.wav',help='Filename of output')
args = parser.parse_args()


outfile = args.output
#outfile = 'final_out.wav'
loadfile = args.input
#loadfile = 'sample.txt'



# Given the output of the RNN, this converts it back into an audio file.

time_step = .05
sampFreq = 44100
window_size = int(time_step * sampFreq)
print(window_size)

# load saved DFT
print("Loading file: " + loadfile)
dft = np.loadtxt(loadfile)
num_windows,output_size = dft.shape


print(dft.shape)
print(window_size)
dft = dft.reshape(num_windows,2,window_size)

# inverse DFT
out = np.empty((num_windows*window_size))
for i in range(0,num_windows):
  # convert magnitude-angle pairs back to complex numbers, then take inverse FT
  inverse = np.fft.ifft(dft[i][0] * np.exp(1j * dft[i][1]))
  # scale back up from (-1,1) range to 16 bit
  out[i*window_size:(i+1)*window_size] = inverse * (2. ** 15)
  
out = np.round(out) # makes float->int conversion better
out = out.astype(np.int16)
wavfile.write(outfile,44100,out) #TODO - don't hard code sample frequency here (44100)
