# from pylab import *
import numpy as np
from scipy.io import wavfile

testing = False

# This file performs a Discrete Fourier Transform (DFT) on the below file,
# saving is as a .npy file.
# The array of the DFT saved in each .npy file is a 3D array with dimensions
# (num_windows, 2, window_size). The first dimension corresponds to the 
# number of discrete windows used to take the DFT. These windows each have
# size window_size. The second dimension corresponds to the magnitudes and 
# angles of the waveforms - the magnitudes are saved at index 0, and the 
# angles at index 1.
# IS THE ABOVE UP TO DATE?

# The below files should all be single-channel, 16 bit, signed
#f = '/home/luke/Data/Bach/oneC.wav'
f = 'data/trim.wav'

# name of file we will save with the dft transform of f.
save = 'data/dft.npy'

sampFreq, data = wavfile.read(f) # read .wav file

# # we take the discrete fourier transform with time interval time_step (seconds)
# sampFreq will be 44100
time_step = .05
window_size = int(time_step * sampFreq) # = 2205


data = np.trim_zeros(data) # trim 0s from start and end of audio file
ndata = data / (2.**15) # convert to floats in range (-1,1)

# windows will overlap by half
num_windows = ndata.shape[0] // int(window_size / 2) # // is int division (instead of float)
dft = np.empty([num_windows, 2, window_size]) 

# do DFT, save magnitudes and angles of frequency vector components
i = 0
window = 0
overlap = 1 # num of overlapping windows. If 1, then no overlap.
while i < ndata.shape[0] - window_size:
  transform = np.fft.fft(ndata[i:i+window_size])
  dft[window][0] = np.absolute(transform)
  dft[window][1] = np.angle(transform)
  i += window_size / overlap
  window += 1

# here window_size is 10584

# Reshape
dft = dft.reshape((num_windows,2*window_size))
dft = dft[:-32,:] # throw out the last eight seconds or so
# Truncate data! to make testing faster!
#dft = dft[:200,:]

print("Shape of final data:")
print(dft.shape)

np.save(save,dft)

