-------------------------------------------------------------------------------
-- loads opt.data into torch tensor and saves to file. Will load 
-- from this second file on subsequent calls.
-------------------------------------------------------------------------------

require 'torch'   -- torch
require 'image'   -- for color transforms
require 'nn'      -- provides a normalization operator
require 'read_options'
npy4th = require 'npy4th' -- convert .npy files to tensors
if opt.useCuda > 0 then
  require 'cunn'
end


----------------------------------------------------------------------
print '==> loading dataset'

-- filenames of torch tensors of data
data_file = 'data/data.t7'
dft_npy_file = 'data/dft.npy'


if paths.filep(data_file) then
  data = torch.load(data_file)
else

  local fulldata
  -- set simpletest = true for a sanity check that LSTMs are working with
  -- regression
  simpletest = false
  randomtest = true
  if simpletest then
    fulldatalength = 10000
    fulldata = torch.Tensor(fulldatalength,1)
    for i=1,fulldatalength do
      if i%2 == 0 then
        fulldata[i][1] = 1
      elseif i%3 == 0 then
        fulldata[i][1] = 1
      else
        fulldata[i][1] = 2
      end
    end
  elseif randomtest then
    fulldatalength = 10000
    fulldata = torch.Tensor(fulldatalength,1)
    for i=1,fulldatalength do
      if torch.random(2) == 2 then
        fulldata[i][1] = 2
      else
        fulldata[i][1] = 1
      end
    end
  -- load compressed representation of audio from autoencoder
  elseif opt.compression_file ~= 'nil' then
    print("Loading data from autoencoder compression file...")
    fulldata = torch.load(opt.compression_file)
    print(#fulldata)
  -- otherwise, need to load from the file made from load_data.py
  else
    print("Loading data from DFT file...")
    fulldata = npy4th.loadnpy(dft_npy_file)
  end

  -- loaded data has dimension num_windows x 2 x data.windowSize.
  -- truncate so the number of windows is divisible by the batch size:
  -- -1...+1 is because targets are shifted over by one
  seq_batch_size = opt.batchSize * opt.seqLength
  fulldata = fulldata:narrow(1,1,(#fulldata)[1] - (((#fulldata)[1]-1) % seq_batch_size))

  -- concatenate magnitudes and angles, treat as single input.
  -- split into training and validation
  local endTrain = math.floor((1-opt.val) * (#fulldata)[1])
  local endTrain = endTrain - (endTrain % (seq_batch_size)) -- round to batch size

  print(#fulldata)

  data = {
    fulldata=fulldata,
    inputs = fulldata:narrow(1,1,(#fulldata)[1]-1),
    targets = fulldata:narrow(1,2,(#fulldata)[1]-1),
    size = ((#fulldata)[1]-1),
    endTrain = endTrain,
    windowSize = (#fulldata)[2]
  }

  torch.save(data_file, data) 

  -- now, serialize x. Will load with a separate python file to convert to
  -- audio.
  io.output(opt.save .. '/test.out')
  for i=1,data.size do
    for j=1,data.windowSize do
      io.write(data.fulldata[i][j])
      io.write(' ')
    end
    io.write('\n')
  end
  io.flush()
  io.output(io.stdout)
end


