require 'rnn'
require 'minibatch'
require '1_data'

-- require 'vocab'
function sample(model,length)
  -- need this, because we are currently remembering stuff specific to batch 
  -- processing
  model:forget() 
  model:evaluate() -- set in evaluation mode
  s = '' -- start with empty string


  print("Seed:")
  seedlen = 40


  
  -- get first element of sequence
  local x, _ = makebatch(data.inputs,data.targets,1,1,1)
  seq = {x[1]}
  for t = 1, seedlen do
    model:forward({seq[t]})
    seq[t+1], _ = makebatch(data.inputs,data.targets,t,1,1)[1]
  end


  print("Generated sequence:")
  for t=seedlen+1,seedlen+length do
    seq[t+1] = model:forward({seq[t]})[1]:clone()
  end

  --    print(seq)
  -- print_seq = true
  -- if print_seq then
  --   -- Print the sequence out
  --   for i=1,seedlen+length do
  --     print(seq[i][1])
  --   end
  -- end
  

  return seq

end

  

model = torch.load('results/mix1/model.net')
length = 200

seq = sample(model,length)


-- now, serialize x. Will load with a separate python file to convert to
-- audio.

io.output('sample.txt')
for i=1,#seq do
  for j=1,data.windowSize do
    io.write(seq[i][1][j])
    io.write(' ')
  end
  io.write('\n')
end
io.flush()
io.output(io.stdout)
